<?
$MESS["IBLOCK_ID"]                  = "ID инфоблока";
$MESS["SECTIONS_ID"]                = "Загружаемые разделы";
$MESS["IBLOCK_TYPE"]                = "Типы инфоблоков";
$MESS["FILES_EXPORT_PATH"]          = "Путь к сохраняемым данным";
$MESS["DATA_FILE_NAME_TEMPLATE"]    = "Шаблон сохранения данных";
$MESS["DATA_EXPORT_TYPE"]           = "Тип для сохранения данных (json/xml)";
$MESS["FILES_SOURCE_PATH"]          = "Источник данных для экспорта";
$MESS["FILES_EXPORT_PATH"]          = "Путь назначения сохранения";
$MESS["PARSING_SITE"]               = "Сайт для загрузки";