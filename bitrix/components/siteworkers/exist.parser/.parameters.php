<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader,
    Bitrix\Main\Web\Json,
    Bitrix\Iblock,
    Bitrix\Catalog,
    Bitrix\Currency;

global $USER_FIELD_MANAGER;

if (!Loader::includeModule('iblock'))
    return;

$arIBlockType = CIBlockParameters::GetIBlockTypes();

$arIBlock = array();
$ibFilter = !empty($arCurrentValues['IBLOCK_TYPE'])
    ? array('TYPE' => $arCurrentValues['IBLOCK_TYPE'], 'ACTIVE' => 'Y')
    : array('ACTIVE' => 'Y');

$rsIBlock = CIBlock::GetList(array('SORT' => 'ASC'), $ibFilter);
while ($arr = $rsIBlock->Fetch())
{
    $id = (int)$arr['ID'];
    if (isset($offersIblock[$id]))
        continue;
    $arIBlock[$id] = '['.$id.'] '.$arr['NAME'];
}

$sections = array();

$arFilter = Array('IBLOCK_ID' => IntVal($arCurrentValues["IBLOCK_ID"]), 'GLOBAL_ACTIVE'=>'Y');
$res = CIBlockSection::GetList(array(), $arFilter, true);
while($arFields = $res->GetNext())
{
    $sections[$arFields["ID"]] = '['.$arFields["ID"].'] '.$arFields['NAME'];
}

$arComponentParameters = array(
    'PARAMETERS' => array(
        'IBLOCK_TYPE' => array(
            'PARENT' => 'BASE',
            'NAME' => GetMessage('IBLOCK_TYPE'),
            'TYPE' => 'LIST',
            'VALUES' => $arIBlockType,
            'REFRESH' => 'Y',
        ),
        'IBLOCK_ID' => array(
            'PARENT' => 'BASE',
            'NAME' => GetMessage('IBLOCK_ID'),
            'TYPE' => 'LIST',
            'VALUES' => $arIBlock,
            'REFRESH' => 'Y',
        ),
        'SECTIONS' => array(
            'PARENT' => 'BASE',
            'NAME' => GetMessage('SECTIONS_ID'),
            'TYPE' => 'LIST',
            'MULTIPLE' => 'Y',
            'ADDITIONAL_VALUES' => 'Y',
            'VALUES' => $sections,
        ),
        'PARSING_SITE' => array(
            'PARENT' => 'BASE',
            'NAME' => GetMessage('PARSING_SITE'),
            'TYPE' => 'STRING',
            'VALUES' => "",
            'DEFAULT' => "https://www.exist.ru",
        ),
    ),
);