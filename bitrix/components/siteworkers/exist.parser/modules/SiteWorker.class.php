<?php
namespace Bitrix\Main\tecdoc\parser;
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Main,
    Bitrix\Iblock,
    CUtil;

class SiteWorker
{
    private $params = array();
    private $arParams = array();
    private $articleCode = "";

    function __construct($params, $arParams)
    {
        $this->articleCode = "CML2_ARTICLE";
        $this->params = $params;
        $this->arParams = $arParams;
    }

    public function execute($data) {
        if (isset($this->arParams["IBLOCK_ID"])) {
            $resultData = array();
            foreach ($data as $dt) {
                if (isset($resultData[$dt["id"]])) {
                    continue;
                }

                $resultData[$dt["id"]] = array();
                if (!empty($dt["params"]) || !empty($dt["image"]) || !empty($dt["prev_text"]) || !empty($dt["applicability"])) {
                    foreach ($dt as $d => $t) {
                        $resultData[$dt["id"]][$d] = $t;
                    }
                }
            }

            foreach ($resultData as $r => $dt) {
                if ($dt) {
                    $this->updateElements($dt);
                }
            }
        }
    }

    /*
     * $data = array("PROP_NAME" => "VALUE")
     */
    private function updateElements($data) {
        $tryArticle = true;
        $tryName    = false;

        $arSelect = Array("ID", "NAME", "DATE_ACTIVE_FROM", "PROPERTY_CML2_ARTICLE");
        $arFilter = Array("ID" => $data["id"], "IBLOCK_ID" => IntVal($this->arParams["IBLOCK_ID"]), "SECTION_ID" => $this->arParams["SECTIONS"], "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
        $res = \CIBlockElement::GetList(Array(), $arFilter, false, array(), $arSelect);
        while ($arFields = $res->GetNext()) {
            $siteProperty = preg_replace('/[\s\-\_\+\-\(\)\=\*\$\#\@\!\:\;\.\,]/', '', $arFields["PROPERTY_" . $this->articleCode . "_VALUE"]);
            if ($tryArticle) {
                $this->insertData($arFields, $data);
                $tryName = false;
            }

            if ($tryName) {
                foreach ($data["elements"] as $element) {
                    if (isset($element["name"])) {
                        $siteProperty = preg_replace('/[\s\-\_\+\-\(\)\=\*\$\#\@\!\:\;\.\,]/', '', $element["name"]);
                        $this->insertData($arFields, $data);
                    }
                }
            }
        }

        return false;
    }


    private function insertData($element, $data)
    {
        $ibElement = new \CIBlockElement();
        $props = array();

        $insertProps = false;
        if ($insertProps)
        {
            $this->insertProperties($element, $data["params"]);


            $props = array();
            foreach ($data["params"] as $p => $param) {
                $propCode = Cutil::translit($p, "ru", array(
                    "change_case" => "U",
                ));

                $props["PRM_".$propCode] = $param;
            }
        }

        $props[str_replace("PROPERTY_", "", "PROPERTY_CML2_ARTICLE")] = $element["PROPERTY_CML2_ARTICLE_VALUE"];
        $props["CML2_ARTICLE"] = $element["PROPERTY_CML2_ARTICLE_VALUE"];

        $props["IS_UPDATED"] = 2;

        $imageName = preg_replace('/[\s\-\_\+\-\(\)\=\*\$\#\@\!\:\;\,]/', '', stristr($data["image"], ".JPG") ? $data["name"].".jpg" : $data["name"].".png");

        $detailImageLink = $_SERVER["DOCUMENT_ROOT"]."/upload/parser/".$data["id"]."_detail.jpg";

        $image = preg_replace("/[&][a-zA-Z]+[=][0-9]+[x][0-9]+/", "",$data["image"]);

        $ch = curl_init($image);
        $fp = fopen($detailImageLink, 'wb');
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_exec($ch);
        curl_close($ch);
        fclose($fp);

        imagejpeg(imagecreatefromstring(file_get_contents($data["image"])), $detailImageLink);

        $prevImageLink   = $_SERVER["DOCUMENT_ROOT"]."/upload/parser/".$data["id"]."_prev.jpg";

        $ch = curl_init($data["image"]);
        $fp = fopen($prevImageLink, 'wb');
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_exec($ch);
        curl_close($ch);
        fclose($fp);

        imagejpeg(imagecreatefromstring(file_get_contents($data["image"])), $prevImageLink);

        $paramTable = $this->getParamTableBlock($data["params"]);
        $applTable  = $this->getParamTableBlock($data["applicability"]);

        $detail = "";

        if ($data["prev_text"]) {
            $detail .= $data["prev_text"]."<br><br>";
        }

        if ($paramTable) {
            $detail .= $paramTable."<br><br>";
        }

        if ($applTable) {
            $detail .= $applTable;
        }

        /*if (isset($data["applicability"]) && !empty($data["applicability"])) {
            $appArray = array();

            foreach ($data["applicability"] as $applic) {
                foreach ($applic as $a => $appl) {
                    if (!in_array($appl["model"], $appArray)) {
                        $appArray[] = $appl["model"];
                    }
                }
            }

            //$ibElement->SetPropertyValues($element["ID"], $this->params["iblock_id"], $appArray, "APPLICABILITY");
            $props["APPLICABILITY"] = $appArray;
        }

        if (isset($data["applicability"]) && !empty($data["applicability"])) {
            $appArray = array();

            foreach ($data["applicability"] as $a => $applic) {
                $applic = get_object_vars($applic);
                $appArray[$a]["VALUE"] = $applic["fulldescription"];
                $appArray[$a]["DESCRIPTION"] = $applic["constructioninterval"];
            }

            //$ibElement->SetPropertyValues($data["id"], $this->iblock, $appArray, "APPLICABILITY");
            $props["APPLICABILITY"] = $appArray;
        }
        */

        if (isset($data["applicability"]) && !empty($data["applicability"])) {
            $appArray = array();
            $ch = 0;
            foreach ($data["applicability"] as $applic) {
                foreach ($applic as $a => $appl) {
                    if (!in_array($appl["model"], $appArray)) {
                        $appArray[$ch]["VALUE"]       = $appl["model"];
                        $appArray[$ch]["DESCRIPTION"] = $appl["period"];

                        ++$ch;
                    }
                }
            }

            //$ibElement->SetPropertyValues($element["ID"], $this->params["iblock_id"], $appArray, "APPLICABILITY");
            $props["APPLICABILITY"] = $appArray;
        }

        if (isset($data["params"]) && !empty($data["params"])) {
            $appArray = array();
            $ch = 0;
            foreach ($data["params"] as $a => $applic) {
                $appArray[$ch]["VALUE"]       = $a;
                $appArray[$ch]["DESCRIPTION"] = $applic;

                ++$ch;
            }

            //$ibElement->SetPropertyValues($element["ID"], $this->params["iblock_id"], $appArray, "APPLICABILITY");
            $props["PARAMETERS"] = $appArray;
        }

        $arLoadProductArray = Array(
            "ACTIVE" => "Y",
            "PREVIEW_TEXT_TYPE" => "html",
            "PREVIEW_TEXT" => (isset($data["prev_text"])) ? $data["prev_text"] : "",
            "DETAIL_TEXT_TYPE" => "html",
            "DETAIL_TEXT" => (!empty($detail)) ? $detail : "",
            "PROPERTY_VALUES"=> (!empty($props)) ? $props : "",
            "PREVIEW_PICTURE" => (isset($data["image"])) ? \CFile::MakeFileArray($prevImageLink) : "",
            "DETAIL_PICTURE" => (isset($data["image"])) ? \CFile::MakeFileArray($detailImageLink) : "",
        );

        $elem = $ibElement->Update($element["ID"], $arLoadProductArray);

        return true;
    }

    /*
     * Формирует таблицу с параметрами товара в виде тега table
     */
    private function getParamTableBlock($paramArray) {
        $strResult = "";
        $strResult .= "<table class='param-table'>";

        foreach ($paramArray as $p => $param) {
            if (!is_array($param)) {
                $strResult .= "<tr class='param-table-row'>";
                $strResult .= "<td class='param-table-col'>" . $p . "</td><td class='param-table-col'>" . $param . "</td>";
                $strResult .= "</tr>";
            } else {
                foreach ($param as $prm) {
                    $strResult .= "<tr class='param-table-row'>";
                    foreach ($prm as $a => $p) {
                        $strResult .= "<td class='param-table-col'>" . $a . "</td><td class='param-table-col'>" . $p . "</td>";
                    }
                    $strResult .= "</tr>";
                }
            }
        }

        $strResult .= "</table>";

        return $strResult;
    }

    private function insertProperties($element, $props)
    {
        $elem = new \CIBlockElement();

        foreach ($props as $p => $prop) {
            $propCode = Cutil::translit("PRM_".$p, "ru", array(
                "change_case" => "U",
            ));

            $isProp = false;

            $prp = $elem->GetProperty($this->params["iblock_id"], $element["ID"], array(), array(), array("NAME" => $propCode));
            while ($pr = $prp->GetNext()) {
                if ($propCode === $pr["CODE"]) {
                    $isProp = true;
                    break;
                }
            }

            if (!$isProp) {
                $arFields = Array(
                    "NAME" => $p,
                    "ACTIVE" => "Y",
                    "SORT" => "500",
                    "CODE" => $propCode,
                    "PROPERTY_TYPE" => "S",
                    "IBLOCK_ID" => $this->params["iblock_id"],
                );

                $ibp = new \CIBlockProperty;
                $propID = $ibp->Add($arFields);
            }
        }
    }
}