<?php
namespace Bitrix\Main\tecdoc\parser;

class Queries {
    private $ch = false;

    function __construct()
    {
        $this->ch = curl_init();
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->ch, CURLOPT_HTTPGET, true);
        curl_setopt($this->ch, CURLOPT_FRESH_CONNECT, 1);
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, true);
    }

    public function getData($address) {
        $address = str_replace ( ' ', '%20', $address);

        curl_setopt($this->ch, CURLOPT_URL, $address);

        if (curl_exec($this->ch)) {
            $info = curl_getinfo($this->ch);
            switch ($info["http_code"]) {
                case "200":
                    $content = curl_multi_getcontent($this->ch);
                    curl_close($this->ch);
                    return $content;

                    break;
                case "401": //Не заполнены поля
                    curl_close($this->ch);
                    throw new \Exception();

                    break;
                case "403": //Пользователь не авторизован
                    curl_close($this->ch);
                    throw new \Exception();

                    break;
                case "301": //Перемещен
                    curl_close($this->ch);
                    throw new \Exception();

                    break;
                default: //Пустой запрос
                    curl_close($this->ch);
                    throw new \Exception();
            }
        } else {
            throw new \Exception();
        }
    }

}