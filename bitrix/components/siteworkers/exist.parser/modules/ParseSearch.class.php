<?php
namespace Bitrix\Main\tecdoc\parser;

class ParseSearch
{
    private $arParams = array();
    private $params;
    private $list;
    private $pages;

    function __construct($params, $arParams)
    {
        $this->arParams     = $arParams;
        $this->params       = $params;
        $this->list         = array();
        $this->items        = array();
    }

    private function getElementsByClass(&$parentNode, $className, $tagName = "div", $findChild = true)
    {
        $nodes = array();

        if (isset($parentNode->parentNode->parentNode)) {
            @$rootNodes = $parentNode->parentNode->parentNode->getElementsByTagName($tagName);
        } else if (isset($parentNode->parentNode)) {
            @$rootNodes = $parentNode->parentNode->getElementsByTagName($tagName);
        } else {
            @$rootNodes = $parentNode->getElementsByTagName($tagName);
        }

        if (isset($rootNodes->length) && ($rootNodes->length > 0)) {
            for ($i = 0; $i < $rootNodes->length; $i++) {
                $temp = $rootNodes->item($i);
                if (($temp->nodeType === 1) && (stripos($temp->getAttribute('class'), $className) !== false)) {
                    $nodes[] = $temp;
                }
            }
        } else {
            if ($findChild && $parentNode->hasChildNodes()) {
                for ($i = 0; $i < $parentNode->childNodes->length; ++$i) {
                    $node = $parentNode->childNodes[i];
                    if (($node->nodeType === 1) && ($node->getAttribute("class") === $className)) {
                        $nodes[] = $node;
                    }
                }
            }
        }

        return $nodes;
    }

    private function getElementsById(&$parentNode, $id, $tagName = "div", $findChild = true)
    {
        $nodes = array();

        @$rootNodes = $parentNode->getElementsByTagName($tagName);
        if (isset($rootNodes->length) && ($rootNodes->length > 0)) {
            for ($i = 0; $i < $rootNodes->length; $i++) {
                $temp = $rootNodes->item($i);
                if (($temp->nodeType === 1) && (stripos($temp->getAttribute('id'), $id) !== false)) {
                    $nodes[] = $temp;
                }
            }
        } else {
            if ($findChild && $parentNode->hasChildNodes()) {
                for ($i = 0; $i < $parentNode->childNodes->length; ++$i) {
                    $node = $parentNode->childNodes[i];
                    if (($node->nodeType === 1) && ($node->getAttribute("id") === $id)) {
                        $nodes[] = $node;
                    }
                }
            }
        }

        return $nodes;
    }

    private function getElementsByTag(&$parentNode, $tag)
    {
        if ($tag === "tr") {
            $point = false;
        }

        $nodes = array();

        $node = @$parentNode->getElementsByTagName($tag);

        if (isset($node->length) && ($node->length > 0)) {
            foreach ($node as $nd) {
                $nodes[] = $nd;
            }
        } else {
            $nodes[] = $node;
        }

        return $nodes;
    }

    private function getElementType($element) {
        switch (substr($element, 0, 1)) {
            case ".":
                return "class";
                break;
            case "#":
                return "id";
                break;
            case (stripos("QWERTYUIOPASDFGHKLZXCVBNMqwertyuiopasdfghjklzxcvbnm", substr($element, 0, 1)) !== false):
                return "tag";
                break;
            default:
                return false;
        }

        return false;
    }

    private function getElements($page, $nodeName, $tagName = "div")
    {
        $type = $this->getElementType($nodeName);
        switch($type)
        {
            case "id":
                $nodeName = str_replace("#", "", $nodeName);
                return $this->getElementsById($page, $nodeName, $tagName);
                break;
            case "class":
                $nodeName = str_replace(".", "", $nodeName);
                return $this->getElementsByClass($page, $nodeName, $tagName);
                break;
            case "tag":
                return $this->getElementsByTag($page, $tagName);
                break;
            default:
                return array();
        }

        return false;
    }

    private function getData($address) {
        try {
            $qrObject = new Queries();
            $data = $qrObject->getData($address);
            $dom = new \DOMDocument();
            @$dom->loadHTML("<?xml encoding='UTF-8'>".$data);

            return $dom;
        }
        catch (\Exception $e)
        {
            return false;
        }
    }

    /*
     * Парсит путь до элементов страницы, запустить в начале, передать корневой раздел и надоб параметров-путь до элемента
     */
    private function parsePath($page, $paramArray) {
        if (isset($paramArray["path"])) {
            $paramArray = $paramArray["path"];
        }

        $roots = array();

        foreach ($paramArray as $p => $path) {
            if ($p === 0) {
                $roots = $this->getElements($page, current($path), key($path));
            } else {
                $temp = array();
                foreach ($roots as $root) {
                    $temp[] = $this->getElements($root, current($path), key($path));
                }
                $roots = array();

                foreach ($temp as $tmp) {
                    if (is_array($tmp)) {
                        foreach ($tmp as $current) {
                            $roots[] = $current;
                        }
                    } else if (is_object($tmp)) {
                        $roots[] = $tmp;
                    }
                }
            }
        }

        return $roots;
    }

    /*
     * парсит элементы на детальной странице элемента (картинки, имя, параметры и пр.)
     */
    private function parseItems($node, $paramArray)
    {
        $result = array();
        $roots = array();

        if (isset($paramArray["path"])) {
            $roots = $this->parsePath($node, $paramArray["path"]);
        }

        if (isset($paramArray["targ"])) {
            if (!$roots) {
                $roots = $this->getElements($node, current($paramArray["targ"]), key($paramArray["targ"]));
            } else {
                foreach ($roots as $root) {
                    $roots = $this->getElements($root, current($paramArray["targ"]), key($paramArray["targ"]));
                }
            }
        }

        if (isset($paramArray["attr"])) {
            if ($roots) {
                foreach ($roots as $root) {
                    if (isset($root->nodeType) && ($root->nodeType === 1)) {
                        switch (key($paramArray["attr"])) {
                            case "attribute":
                                $result[] = $root->getAttribute(current($paramArray["attr"]));

                                break;
                            default:
                                if ($root->getAttribute(key($paramArray["attr"])) === (current($paramArray["attr"]))) {
                                    foreach ($root->childNodes as $child) {
                                        if (isset($child->childNodes)) {
                                            if ($child->childNodes->length === 1) {
                                                $result[$child->childNodes[0]->nodeValue] = "";
                                            } else if ($child->childNodes->length > 1) {
                                                $result[$child->childNodes[0]->nodeValue] = $child->childNodes[1]->nodeValue;
                                            }
                                        } else {
                                            if (isset($child->nodeValue) && !empty($child->nodeValue)) {
                                                $result[] = $child->nodeValue;
                                            }
                                        }
                                    }
                                }
                        }
                    }
                }
            }
        }

        return $result;
    }

    private function parseList($dom, $paramArray, $article)
    {
        $arResult = array();

        $roots = array();
        if (isset($paramArray["list"]["path"])) {
            $roots = $this->parsePath($dom, $paramArray["list"]["path"]);
        }

        if (isset($paramArray["list"]["link"]) && isset($paramArray["list"]["attr"])) {
            foreach ($roots as $r => $root) {
                $artArray = $this->getElements($root, current($paramArray["list"]["article"]), key($paramArray["list"]["article"]));

                $elements = $this->getElements($root, current($paramArray["list"]["link"]), key($paramArray["list"]["link"]));
                foreach ($elements as $e => $element) {
                    if (isset($element->nodeType) && ($element->nodeType === 1)) {
                        $attr = $element->getAttribute(current($paramArray["list"]["attr"]));
                        if ($attr) {
                            if (!stristr($attr, "Parts/Float.aspx")) {
                                $attr = str_replace("Parts.axd", "Parts/Float.aspx", $attr);
                            }

                            if (!isset($this->list[$article["PROPERTY_CML2_ARTICLE_VALUE"]])) {
                                $this->list[$article["PROPERTY_CML2_ARTICLE_VALUE"]] = array(
                                    "ID" => $article["ID"],
                                    "PROPERTY_CML2_ARTICLE" => $article["PROPERTY_CML2_ARTICLE_VALUE"],
                                    "LINK" => $this->arParams["PARSING_SITE"].$attr,
                                    "PAGE" => $article["PROPERTY_CML2_ARTICLE"]
                                );
                            }
                        }
                    }
                }
            }
        }
    }

    public function parsePages($elements, $params, $sch) {
        $paramArray = $params["element"];

        foreach ($elements as $l => $list) {
            if (!isset($this->items[$sch])) {
                $this->items[$sch] = $this->getElementStructure();
            }

            $link = $this->params["parsing_site"] . "" . $list["LINK"];
            $page = $this->getData($link);
            //$page = $list["PAGE"];

            $this->items[$sch]["id"] = $list["ID"];

            if ($page && isset($paramArray["path"])) {
                $roots = array();
                if (isset($paramArray["path"])) {
                    $roots = $this->parsePath($page, $paramArray["path"]);
                }

                foreach ($roots as $r => $root) {
                    if (isset($root->childNodes)) {
                        if (isset($paramArray["image"])) {
                            $imageArray = $this->parseItems($root, $paramArray["image"]);
                            if ($imageArray) {
                                $this->items[$sch]["image"] = $this->parseImage($imageArray);
                            }
                        }

                        if (isset($paramArray["prev_text"])) {
                            $prevTextArray = $this->parseItems($root, $paramArray["prev_text"]);
                            if ($prevTextArray) {
                                $this->items[$sch]["prev_text"] = $this->parsePrevText($prevTextArray);
                            }
                        }

                        if (isset($paramArray["params"])) {
                            $propArray = $this->parseItems($root, $paramArray["params"]);
                            if ($propArray) {
                                $this->items[$sch]["params"] = $this->parseProps($propArray);
                            }
                        }

                        if (isset($paramArray["applicability"])) {
                            $propArray = $this->parseItems($root, $paramArray["applicability"]);
                            if ($propArray) {
                                foreach ($propArray as $propLink) {
                                    $propLink = str_replace("//","", $propLink);
                                    $appArray = $this->parseApplicability($propLink);
                                    $this->items[$sch]["applicability"] = $appArray;
                                }
                            }
                        }
                    }

                    if (empty($this->items[$sch]["image"])) {
                        unset($this->items[$sch]);
                    }
                }
            }
        }
    }

    public function parsePage($paramArray, $page, $sch)
    {
        $items = array($sch => array("id" => $sch));

        $page = $this->getData($page);

        if ($page && isset($paramArray["path"])) {
            $roots = array();
            if (isset($paramArray["path"])) {
                $roots = $this->parsePath($page, $paramArray["path"]);
            }

            foreach ($roots as $r => $root) {
                if (isset($root->childNodes)) {
                    if (isset($paramArray["image"])) {
                        $imageArray = $this->parseItems($root, $paramArray["image"]);
                        if ($imageArray) {
                            $items[$sch]["image"] = $this->parseImage($imageArray);
                        }
                    }

                    if (isset($paramArray["prev_text"])) {
                        $prevTextArray = $this->parseItems($root, $paramArray["prev_text"]);
                        if ($prevTextArray) {
                            $items[$sch]["prev_text"] = $this->parsePrevText($prevTextArray);
                        }
                    }

                    if (isset($paramArray["params"])) {
                        $propArray = $this->parseItems($root, $paramArray["params"]);
                        if ($propArray) {
                            $items[$sch]["params"] = $this->parseProps($propArray);
                        }
                    }

                    if (isset($paramArray["applicability"])) {
                        $propArray = $this->parseItems($root, $paramArray["applicability"]);
                        if ($propArray) {
                            foreach ($propArray as $propLink) {
                                $propLink = str_replace("//","", $propLink);
                                $appArray = $this->parseApplicability($propLink);
                                $items[$sch]["applicability"] = $appArray;
                            }
                        }
                    }
                }

                if (empty($this->items[$sch]["image"])) {
                    unset($this->items[$sch]);
                }
            }
        }

        return $items;
    }

    private function parseApplicability($src) {
        $catSites[] = "elcats.ru";
        $catSites[] = "japancats.ru";

        $appArray = array();
        $links = array();

        $firstName = "";

        $node = $this->getData($src);
        $roots = $this->getElements($node, "#lblModels", "span");
        foreach ($roots as $root) {
            if (isset($root->firstChild)) {
                $firstName = $root->firstChild->nodeValue;
            } else {
                $firstName = "UNTITLED";
            }

            $tags = $this->getElements($root, "", "a");
            foreach ($tags as $tag) {
                if ($tag->nodeType === 1) {
                    $links[$tag->nodeValue] = /*$catSite . "/" . */$tag->getAttribute("href");
                }
            }
        }

        $ch = 0;
        $roots = $this->getElements($node, "#tblTable", "table");
        foreach ($roots as $root) {
            $trs = $this->getElements($root, "", "tr");
            foreach ($trs as $r => $tr) {
                if ($r > 0) {
                    $tds = $this->getElements($tr, "", "td");
                    if ($tds) {
                        $appArray[$firstName][$r]["model"] = $tds[0]->nodeValue;
                        $appArray[$firstName][$r]["engine"] = $tds[1]->nodeValue;
                        $appArray[$firstName][$r]["period"] = $tds[2]->nodeValue;
                        $appArray[$firstName][$r]["info"] = $tds[3]->nodeValue;

                        $ch = $r;
                    }
                }
            }
        }

        ++$ch;

        //$appArray = array();
        if ($links) {
            foreach ($links as $l => $link) {
                $appArray[$l] = array();

                $node = false;
                foreach ($catSites as $catSite) {
                    $node = $this->getData($catSite."/"."".$link);
                    if ($node) {
                        break;
                    }
                }
                if ($node) {
                    //$roots = $this->getElements($node, "#tblTable", "table");

                    //$node->childNodes[2]->childNodes[1]->childNodes[2]->nodeName  //form
                    //$node->childNodes[2]->childNodes[1]->childNodes[2]->childNodes[5]->firstChild->firstChild->childNodes[1]->childNodes[0]->childNodes[4]->childNodes[1]->nodeName  //table

                    /*$roots = $this->getElements($node, "#lblModels", "span");
                    foreach ($roots as $root) {
                        $trs = $this->getElements($root, "", "tr");
                        foreach ($trs as $r => $tr) {
                            if ($r > 0) {
                                $tds = $this->getElements($tr, "", "td");
                                if ($tds) {
                                    $appArray[$l][$ch]["model"] = $tds[0]->nodeValue;
                                    $appArray[$l][$ch]["engine"] = $tds[1]->nodeValue;
                                    $appArray[$l][$ch]["period"] = $tds[2]->nodeValue;
                                    $appArray[$l][$ch]["info"] = $tds[3]->nodeValue;

                                    ++$ch;
                                }
                            }
                        }
                    }*/
                }
            }
        }

        foreach($catSites as $catSite) {
            $point = false;
        }

        return $appArray;
    }

    private function parseProps($nodeArray) {
        $arResult = array();

        foreach ($nodeArray as $n => $node) {
            if ($node) {
                switch ($n) {
                    case (strpos($n, "Индекс нагрузки")):
                        $arResult["Индекс нагрузки"] = $node;
                        break;
                    case (strpos($n, "Индекс скорости")):
                        $arResult["Индекс скорости"] = $node;
                        break;
                    case stristr($n, "Страна производства"):
                        $arResult["Страна производства"] = $node;
                        break;
                    default:
                        $arResult[$n] = $node;
                }
            }
        }

        return $arResult;
    }

    private function parsePrevText($nodeArray)
    {
        $arResult = "";

        foreach ($nodeArray as $n => $node) {
            if ($n === 0) {
                $arResult .= $node;
            } else {
                $arResult .= " ";
                $arResult .= $node;
            }
        }

        return $arResult;
    }

    private function parseImage($nodeArray)
    {
        $result = "";
        foreach ($nodeArray as $node) {
            $result = str_replace("//", "", $node);
            break;
        }

        return $result;
    }

    /*
     * Создает структуру элемента массива
     */
    private function getElementStructure()
    {
        return array(
            "id"        => "",
            "image"     => "",
            "prev_text" => "",
            "params"    => array(),
            "applicability" => array(),
        );
    }

    private function parseSearchLine($line, $delimeter)
    {
        $rsArray = array();
        $posStart = 0;

        if(!stristr($line, $delimeter)) {
            return array($line);
        }

        do {
            $strLen = strlen($line);

            if (!stripos($line, $delimeter)) {
                $posEnd = $strLen;
            } else {
                $posEnd = stripos($line, $delimeter);
            }

            $subBefore = substr($line, 0, ($posEnd + 1));
            $subAfter  = substr($line, ($posEnd + 1));

            $rsArray[] = str_replace($delimeter, "", $subBefore);
            $line = $subAfter;

            if (!stristr($line, $delimeter)) {
                if (!in_array($line, $rsArray)) {
                    $rsArray[] = $line;
                }
            }
        } while(stristr($line, $delimeter));

        return $rsArray;
    }

    public function execute()
    {
        $arSelect = array("ID", "IBLOCK_ID", "IBLOCK_SECTION_ID", "NAME", "DATE_ACTIVE_FROM", "PROPERTY_CML2_ARTICLE","PROPERTY_IS_UPDATED");
        $arFilter = array("ID" => $this->params["array_id"], "IBLOCK_ID"=>IntVal($this->arParams["IBLOCK_ID"]), "SECTION_ID" => $this->arParams["SECTIONS"], "ACTIVE_DATE"=>"Y", "ACTIVE"=>"Y", key($this->params["search_array"]) => current($this->params["search_array"]));
        $res = \CIBlockElement::GetList(array(), $arFilter, false, array(), $arSelect);
        while($arFields = $res->GetNext()) {
            if ($arFields["ID"] !== "9436")
            {
                continue;
            }

            $queryLine = "https://www.exist.ru/price.aspx?pcode=".$arFields["PROPERTY_CML2_ARTICLE_VALUE"];
            $page = $this->getData($queryLine);
            if ($page) {
                $article = array("ID" => $arFields["ID"], "PROPERTY_CML2_ARTICLE_VALUE" => $arFields["PROPERTY_CML2_ARTICLE_VALUE"], "PAGE" => $page);

                $this->parseList($page, $this->params, $article);

                if ($this->list) {
                    $this->parsePages($this->list, $this->params, $arFields["PROPERTY_CML2_ARTICLE_VALUE"]);
                }
            }


            if ($this->items)
            {
                $sw = new SiteWorker($this->params, $this->arParams);
                $sw->execute($this->items);
            }

            $this->list = array();
            $this->items = array();
            sleep(5);
        }
    }
}