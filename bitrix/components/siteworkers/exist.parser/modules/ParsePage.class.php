<?php
namespace Bitrix\Main\tecdoc\parser;

class ParsePage
{
    private $params = array();
    private $blocks = array();
    private $pages  = array();
    private $elements = array();

    function __construct($parameters)
    {
        $this->params = $parameters;
    }

    private function getElementType($element) {
        switch (substr($element, 0, 1)) {
            case ".":
                return "class";
                break;
            case "#":
                return "id";
                break;
            case (stripos("QWERTYUIOPASDFGHKLZXCVBNMqwertyuiopasdfghjklzxcvbnm", substr($element, 0, 1)) !== false):
                return "tag";
                break;
            default:
                return false;
        }

        return false;
    }

    private function getElementsByClass(&$parentNode, $className, $tagName = "div", $findChild = true)
    {
        $nodes = array();

        @$rootNodes = $parentNode->getElementsByTagName($tagName);
        if (isset($rootNodes->length) && ($rootNodes->length > 0)) {
            for ($i = 0; $i < $rootNodes->length; $i++) {
                $temp = $rootNodes->item($i);
                if (stripos($temp->getAttribute('class'), $className) !== false) {
                    $nodes[] = $temp;
                }
            }
        } else {
            if ($findChild && $parentNode->hasChildNodes()) {
                for ($i = 0; $i < $parentNode->childNodes->length; ++$i) {
                    $node = $parentNode->childNodes[i];
                    if ($node->getAttribute("class") === $className) {
                        $nodes[] = $node;
                    }
                }
            }
        }

        return $nodes;
    }

    private function getElementsById(&$parentNode, $id, $tagName = "div", $findChild = true)
    {
        $nodes = array();

        @$rootNodes = $parentNode->getElementsByTagName($tagName);
        if (isset($rootNodes->length) && ($rootNodes->length > 0)) {
            for ($i = 0; $i < $rootNodes->length; $i++) {
                $temp = $rootNodes->item($i);
                if (stripos($temp->getAttribute('id'), $id) !== false) {
                    $nodes[] = $temp;
                }
            }
        } else {
            if ($findChild && $parentNode->hasChildNodes()) {
                for ($i = 0; $i < $parentNode->childNodes->length; ++$i) {
                    $node = $parentNode->childNodes[i];
                    if ($node->getAttribute("id") === $id) {
                        $nodes[] = $node;
                    }
                }
            }
        }

        return $nodes;
    }

    private function getElementsByTag(&$parentNode, $tag)
    {
        $nodes = array();

        $node = @$parentNode->getElementsByTagName($tag);
        $node ? ($nodes[] = $node) : ($nodes = false);

        return $nodes;
    }

    /*
     * Сделать запрос к сайту и получить страницу (DOMDocument)
     */
    private function getData($address) {
        $qrObject = new Queries();
        $data = $qrObject->getData($address);
        $dom = new \DOMDocument();
        @$dom->loadHTML($data);

        return $dom;
    }

    private function getElements($page, $nodeName, $tagName = "div")
    {
        $type = $this->getElementType($nodeName);
        switch($type)
        {
            case "id":
                $nodeName = str_replace("#", "", $nodeName);
                return $this->getElementsById($page, $nodeName, $tagName);
                break;
            case "class":
                $nodeName = str_replace(".", "", $nodeName);
                return $this->getElementsByClass($page, $nodeName, $tagName);
                break;
            case "tag":
                return $this->getElementsByTag($page, $nodeName);
                break;
            default:
                return array();
        }

        return false;
    }

    /*
     * Парсим дерево элементов от корня до потомка
     */
    private function parseTree(&$page, $params)
    {
        $nodes = array();

        if (isset($params["root"]) && isset($params["target"])) {
            $root = $this->getElements($page, $params["root"]);
            if (!empty($root)) {
                foreach ($root as $parent) {
                    $type = $this->getElementType($params["root"]);
                    if ($parent->getAttribute($type) === substr($params["target"], 1)) {
                        $nodes[] = $parent;
                    } else {
                        do {
                            $parent = $parent->firstChild;

                            while ($parent->nodeType !== 1) {
                                if (!isset($parent->nextSibling)) {
                                    break;
                                }
                                $parent = $parent->nextSibling;
                            }

                            if ($parent === null) {
                                break;
                            }

                            if ($parent->nodeType === 1) {
                                $node = @$this->getElements($parent, $params["target"], $parent->tagName);
                                if (count($node) > 0) {
                                    foreach ($node as $nd) {
                                        if ((isset($nd->length) && ($nd->length > 0)) || (count($nd) > 0)) {
                                            if (isset($params["attribute"]) && isset($nd->nodeType) && ($nd->nodeType === 1)) {
                                                if (stripos($nd->getAttribute($params["attribute"]),"//") !== false) {
                                                    $nodes[] = str_replace("//", "", $nd->getAttribute($params["attribute"]));
                                                } else {
                                                    $nodes[] = $nd->getAttribute($params["attribute"]);
                                                }
                                            } else {
                                                $nodes[] = $nd;
                                            }
                                        }
                                    }
                                }
                            } else {
                                $point = false;
                            }
                        } while (count($parent->childNodes) > 0);
                    }
                }
            }
        }

        return $nodes;
    }

    /*
     * Парсим страницы пагинатора
     */
    private function parseBlocks($address) {
        $page = $this->getData($address);

        if ($page) {
            $this->blocks[] = $page;
            $parent = $this->getElements($page, $this->params["pagination_root"],"span");
            foreach ($parent as $p => $prnt) {
                $link = "";
                $i = 0;

                while (empty($link)) {
                    if ($prnt->childNodes[$i]->tagName === $this->params["pagination_current"]) {
                        if (!isset($prnt->childNodes[$i + 1])) {
                            return;
                        }
                        $link = $this->params["parsing_site"] . "" . $prnt->childNodes[$i + 1]->getAttribute("href");
                        break;
                    }
                    ++$i;
                }

                if (!empty($link)) {
                    $this->parseBlocks($link);
                }
            }
        }
    }

    /*
     * Парсим страницы
     */
    private function parsePages()
    {
        foreach ($this->blocks as $b => $block) {
            $pages = $this->getElements($block, $this->params["list_root"]);
            foreach ($pages as $p => $page) {
                $elements = $this->getElements($page, $this->params["list_element"]);
                foreach ($elements as $e => $element) {
                    $this->pages[] = $element;
                }
            }
        }
    }

    private function createElementArray()
    {
        return array(
            "link"          => "",
            "description"   => "",
            "name"          => "",
            "type"          => "",
            "image"         => "",
            "params"        => array()
        );
    }

    /*
     * Парсим элемент
     */
    private function parseElements($paramsArray)
    {
        $inArray = array();

        foreach ($this->pages as $p => $page)
        {
            $root = $this->getElements($page, $this->params["list_element_link"], "a");
            if (count($root) > 0) {
                $inArray[$p] = $this->createElementArray();

                $link = $root[0]->getAttribute("href");

                $inArray[$p]["link"] = $link;

                if (!empty($link)) {
                    $page = $this->getData($this->params["parsing_site"]."".$link);

                    $elements = $this->getElements($page, $this->params["element_info_root"]);
                    foreach ($elements as $e => $element) {
                        if (is_array($this->params["element_description"])) {
                            $description = $this->parseDescription($element, "span");
                            if ($description) {
                                $inArray[$p]["description"] = $description;
                            }
                        }

                        $name = $this->getElements($element, $this->params["element_name"], "h1");
                        if (!empty($name)) {
                            $inArray[$p]["name"] = $name[0]->textContent;
                        }

                        $type = $this->getElements($element, $this->params["element_type"]);
                        if (!empty($type)) {
                            $inArray[$p]["type"] = $type[0]->textContent;
                        }

                        if (is_array($this->params["element_image"])) {
                            $image = $this->parseTree($element, $this->params["element_image"]);
                            if (!empty($image)) {
                                $inArray[$p]["image"] = $image[0];
                            }
                        } else {
                            $image = $this->getElements($element, $this->params["element_image"]);
                            if (!empty($image)) {
                                $inArray[$p]["image"] = $image[0];
                            }
                        }

                        if (is_array($this->params["element_parameters"])) {
                            $params = $this->parseTree($element, $this->params["element_parameters"]);
                            if ($params) {
                                $paramArray = $this->parseParams($params, ".ZeForm item_des");
                                $inArray[$p]["params"] = $paramArray;
                            }
                        } else {
                            $params = $this->getElements($element, $this->params["element_parameters"]);
                            if (!empty($params)) {
                                $inArray[$p]["params"] = $params;
                            }
                        }
                    }
                }
            }
        }

        $arResult = array();
        foreach ($inArray as $arr) {
            if (isset($arr["params"]) && !empty($arr["params"]) && isset($arr["params"]["Артикул"])) {
                $arResult[preg_replace('/[\s\-\_\+\-\(\)\=\*\$\#\@\!\:\;\.\,]/', '', $arr["params"]["Артикул"])] = $arr;
            } else {
                $arResult[] = $arr;
            }
        }
        $this->elements = $arResult;
    }

    private function parseDescription($parent, $params)
    {
        $elements = $parent->getElementsByTagName("span");
        foreach ($elements as $e => $element) {
            if ($element->getAttribute("itemprop") === "description") {
                return $element->nodeValue;
            }
        }

        return false;
    }

    private function parseParams($params, $parent)
    {
        $paramArray = array();

        $type = $this->getElementType($parent);

        foreach ($params[0] as $p => $param)
        {
            $isParent = false;

            switch ($type)
            {
                case "tag":
                    if ($param->parentNode->tagName === $parent)
                    {
                        $isParent = true;
                    }

                    break;
                default:
                    if ($param->parentNode->getAttribute($type) === substr($parent, 1))
                    {
                        $isParent = true;
                    }
            }

            if (!$isParent)
            {
                if (isset($param->childNodes)) {
                    if (strlen($param->nodeValue) < 50) {
                        if (!isset($param->previousSibling)) {
                            $paramArray[$param->nodeValue] = "";
                        } else {
                            if (isset($paramArray[$param->previousSibling->nodeValue])) {
                                $paramArray[$param->previousSibling->nodeValue] = $param->nodeValue;
                            }
                        }

                        foreach ($paramArray as $i => $prm) {
                            if ($i === "Страна производства") {
                                if (stristr($param->parentNode->nodeValue, $i)) {
                                    $paramArray[$i] = $param->childNodes[0]->nodeValue;
                                }
                            }
                        }
                    } else {
                        if (!isset($paramArray[$param->childNodes[0]->textContent]) && (strlen($param->childNodes[0]->textContent) < 50))
                        {
                            $paramArray[$param->childNodes[0]->textContent] = "";
                        }
                    }
                }
            }
        }

        return $paramArray;
    }

    public function execute($address)
    {
        $this->parseBlocks($address);
        $this->parsePages();
        $this->parseElements(array());

        return array(
            "blocks"    => $this->blocks,
            "pages"     => $this->pages,
            "elements"  => $this->elements,
        );
    }


}