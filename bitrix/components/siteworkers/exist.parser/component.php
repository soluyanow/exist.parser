<?
namespace Bitrix\Main\tecdoc\parser;

use Bitrix\Main\Context,
	Bitrix\Main\Loader,
	Bitrix\Main\Type\Collection,
	Bitrix\Main\Type\DateTime,
	Bitrix\Main,
	Bitrix\Currency,
	Bitrix\Catalog,
	Bitrix\Iblock;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$modulesRoot = $_SERVER["DOCUMENT_ROOT"]."/bitrix/components/siteworkers/exist.parser/modules";
include_once($modulesRoot."/ParsePage.class.php");
include_once($modulesRoot."/ParseSearch.class.php");
include_once($modulesRoot."/Queries.class.php");
include_once($modulesRoot."/SiteWorker.class.php");

$parsingFields = array(
    "list"          => array(
        "path"      => array(
            array("div" => "#priceBody"),
            array("div" => ".row-container")
        ),
        "link"      => array("a" => ".row"),
        "article"   => array("div" => ".partno"),
        "attr"      => array("attribute" => "href"),
    ),

    "element"       => array(
        "path"      => array(
            array("div" => ".hproduct"),
            array("div" => ".page-blocks"),
        ),

        "prev_text" => array(
            "targ"  => array("span" => ""),
            "attr"  => array("itemprop" => "description"),
        ),

        "image"     => array(
            "path"  => array(
                array("div" => ".photo"),
                array("div" => "#ctl00_b_ctl00_pnlZoom")
            ),
            "targ"  => array("img" => "#ctl00_b_ctl00_imgMain"),
            "attr"  => array("attribute" => "src"),
        ),

        "params"    => array(
            "path"  => array(
                array("span" => "#ctl00_b_ctl00_bmTabs"),
                array("div" => "#ctl00_b_ctl00_bmTabsC0"),
            ),
            "targ"  => array("div" => ".ZeForm"),
            "attr"  => array("div" => ""),
        ),

        "applicability" => array(
            "path"  => array(
                array("div" => ".hproduct"),
            ),
            "targ"  => array("iframe" => ".iframe-catalog"),
            "attr"  => array("attribute" => "src"),
        ),

        "apptarget" => array(
        ),
    ),
);

$psObject = new ParseSearch($parsingFields, $arParams);
$psObject->execute();